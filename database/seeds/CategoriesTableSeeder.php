<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'name'  => 'T-shirts',
                'image' => '/img/t-shirt.jpg',
            ],
            [
                'name'  => 'Shirts',
                'image' => '/img/shirt.jpg',
            ],
            [
                'name'  => 'Pants',
                'image' => '/img/pants.png',
            ],
        ]);
    }
}
