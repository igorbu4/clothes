<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSwaeters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('categories')->insert([
            [
                'name' => 'Sweaters',
                'image' => '/img/sweater.jpg',
            ],
            [
                'name' => 'Dresses',
                'image' => '/img/dress.jpeg',
            ],
            [
                'name' => 'Skirts',
                'image' => '/img/skirt.jpg',
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('categories')->whereIn('name', ['Sweaters', 'Dresses', 'Skirts'])->delete();
    }
}
