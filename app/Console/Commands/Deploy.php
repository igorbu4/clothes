<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Deploy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = '_deploy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deploy';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        chdir(base_path());
        $commands = [
            'echo $PWD',
            'whoami',
            'git log -1',
            'git pull origin master',
            'git log -1',
            'composer install --no-dev',
            'php artisan migrate --force',
//            'php artisan cache:clear',
//            'php artisan route:cache',
//            'php artisan config:cache',
//            'php artisan view:clear',
        ];
        $output = '';
        foreach ($commands as $command) {
            $tmp = shell_exec($command);
//            $output .= "<span style=\"color: #6BE234;\">\$</span> <span style=\"color: #729FCF;\">{$command}\n</span>";
            $output .= htmlentities(trim($tmp))."\n";
        }
        \Log::info($output);
        echo $output;
    }
}
