<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Item.
 *
 * @property int $id
 * @property string $name
 * @property string $image
 * @property int $category_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Item whereUserId($value)
 * @mixin \Eloquent
 */
class Item extends Model
{
    protected $fillable = ['image', 'category_id', 'user_id'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function usages()
    {
        return $this->hasMany(Usage::class);
    }

    public function getImageRealPath()
    {
        return str_replace('/storage/', storage_path('app/public/'), $this->image);
    }
}
