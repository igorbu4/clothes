<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ItemController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Item::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fname = uniqid('item_', true) . '.' . $request->file('photo')->clientExtension();

        Image::make($request->file('photo')->path())
            ->orientate()
            ->widen(1200, function ($constraint) {
                $constraint->upsize();
            })
            ->heighten(1200, function ($constraint) {
                $constraint->upsize();
            })
            ->save(storage_path('app/public/' . $fname), 90);

        Item::create([
            'image' => '/storage/' . $fname,
            'category_id' => $request->get('category_id'),
            'user_id' => $request->user()->id,
        ]);

        return redirect()->route('categories.show', $request->get('category_id'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Item $item
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        $item->touch();
        $item->usages()->create();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Item $item
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        unlink($item->getImageRealPath());
        $item->delete();

        return redirect()->back();
    }
}
