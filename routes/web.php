<?php

Route::get('/', 'Controller@index');
//Route::get('/offline.html', 'Controller@index');

Auth::routes();
Route::middleware('auth')->group(function () {
    Route::resource('categories', 'CategoriesController');
    Route::resource('items', 'ItemController');

    Route::get('/home', 'CategoriesController@index')->name('home');
});
