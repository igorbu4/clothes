@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <p class="m-2"><b>{{$category->name}}</b></p>
                        <button class="btn" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false"
                                aria-controls="collapseExample">
                            add new item
                        </button>
                    </div>
                    <div class="collapse" id="collapseExample">
                        <form method="post" enctype="multipart/form-data" class="p-4 border border-success"
                              action="{{route('items.store')}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="photo">Photo</label>
                                <input type="file" class="form-control-file" accept="image/jpeg" id="photo"
                                       name="photo" required>
                            </div>
                            <input type="hidden" value="{{$category->id}}" name="category_id"/>
                            <button type="submit" class="btn btn-success">Save</button>
                        </form>
                    </div>
                    <div class="card-body d-flex flex-column">
                        @foreach($items as $item)
                            <div class="card p-1 m-1">
                                <h5 class="text-center">
                                    Uses: <b>{{$item->usages()->count()}}</b>.
                                    Last at <b>{{$item->updated_at->format('d.m.Y H:i:s')}}</b>
                                </h5>
                                <img class="card-img-top" src="{{$item->image}}" alt="item">
                                <div class="card-body p-1">
                                    <div class="clearfix">
                                        <form action="{{route('items.destroy', $item)}}" method="post">
                                            {{method_field('delete')}}
                                            {{csrf_field()}}
                                            <button style="min-width: 85px" class="btn btn-outline-danger float-left"
                                                    type="submit">
                                                REMOVE
                                            </button>
                                        </form>
                                        <form action="{{route('items.update', $item)}}" method="post">
                                            {{method_field('put')}}
                                            {{csrf_field()}}
                                            <button style="min-width: 85px" class="btn btn-outline-success float-right">
                                                USE
                                            </button>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
