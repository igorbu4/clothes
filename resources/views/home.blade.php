@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <p class="m-2">Dashboard</p>
                        <button class="btn" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false"
                                aria-controls="collapseExample">
                            add new item
                        </button>
                    </div>
                    <div class="collapse" id="collapseExample">
                        <form method="post" enctype="multipart/form-data" class="p-4 border border-success" action="{{route('items.store')}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="photo">Photo</label>
                                <input type="file" class="form-control-file" accept="image/jpeg" id="photo" name="photo" required>
                            </div>
                            <div class="form-group">
                                <label for="category">Category</label>
                                <select class="form-control" id="category" name="category_id" required>
                                    <option disabled selected hidden></option>
                                    @foreach($categories_all as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="submit" class="btn btn-success">Save</button>
                        </form>
                    </div>
                    <div class="card-body text-center bg-light">
                        @foreach($categories as $category)
                                <a href="{{route('categories.show', $category)}}">
                                    <div class="m-3 bg-white border-bottom border-right">
                                        <h4 class="p-1">{{$category->name}}</h4>
                                        <img class="p-1" src="{{$category->image}}" style="max-height: 100px">
                                    </div>
                                </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
